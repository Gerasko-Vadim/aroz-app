import React from "react"
import { Htag } from "../../components"
import styles from "./descriptionBlock.module.css"
import VectorSvg from "./Vector.svg"

export const DescriptionBlock = ()=>{
    return(
        <div className={styles.descBlock}>
            <h1 className={styles.title}>Как вступить в Ассоциацию руководителей организаций здравоохранения КР</h1>
            <span className={styles.description}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Condimentum 
                diam orci pretium a pharetra, feugiat cursus. Dictumst risus, sem egestas
                 odio cras adipiscing vulputate. Nisi, risus in suscipit non. Non commodo 
                 volutpat, pharetra, vel.</span>
            <a className={styles.a}>Перейти <VectorSvg/></a>
        </div>
    )
}