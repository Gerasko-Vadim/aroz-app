import { Button, Input } from "antd"
import Search from "antd/lib/input/Search"
import React from "react"
import { ChangeLanguage, Container } from '../../components'
import styles from "./Header.module.css"
import { HeaderProps } from "./Header.props"
import LogoSvg from "./logo.svg"
import {SearchOutlined} from "@ant-design/icons"
import Link from 'next/link'

export const Header = ({ children }: HeaderProps): JSX.Element => {
    const onSearch = ()=>{

    }
    return (
        < >
            <Container>
                <div className={styles.header}>
                    <div className={styles.links}>
                        <LogoSvg />
                        <Link href="/about-us">О нас</Link>
                        <a>Новости</a>
                        <a>НПА</a>
                        <a>Контакты</a>
                        <a>Другое</a>
                    </div>
                    <div className={styles.search}>
                        <ChangeLanguage />
                        <Input
                        style={{
                            height: "40px",
                            maxWidth:"220px",
                            margin: "0px 25px 0px 35px"
                        }}
                        className={styles.inputSearch}
                         placeholder="Поиск" />
                        <Button style={{
                                width: "40px",
                                height: "40px",
                                color: "white",
                                background: "#48887B",
                                borderRadius: "2px",
                        }} icon={<SearchOutlined /> }  />
                    </div>
                </div>
            </Container>

        </>
    )
}