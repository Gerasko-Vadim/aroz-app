import React from "react"
import { ButtonForSidebar } from "../../components/buttonForSidebar/buttonForSidebar"
import styles from "./Sidebar.module.css"
import { SidebarProps } from "./Sidebar.props"

const sidebarImg = "/sidebarImg.png"

export const Sidebar = (): JSX.Element => {
    return (
        <div className={styles.sidebar}>
            <ButtonForSidebar active={true}>
                Устав Центра
            </ButtonForSidebar>
            <ButtonForSidebar>
                Устав Центра
            </ButtonForSidebar>
            <ButtonForSidebar>
                Устав Центра
            </ButtonForSidebar>
            <ButtonForSidebar>
                Устав Центра
            </ButtonForSidebar>
            <ButtonForSidebar>
                Устав Центра
            </ButtonForSidebar>
            <ButtonForSidebar>
                Устав Центра
            </ButtonForSidebar>
            <ButtonForSidebar>
                Устав Центра
            </ButtonForSidebar>
            <div className={styles.line}></div>
            <h2 className={styles.title}>Полезное</h2>
            <img alt="img" src={sidebarImg}/>
        </div>
    )
}