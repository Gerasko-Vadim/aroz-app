import React from "react"
import { Container } from "../components"
import { Footer } from "./Footer/Footer"
import { Header } from "./Header/Header"
import styles from "./layout.module.css"
import { LayoutProps } from "./layout.props"

export const Layout = ({ children }: LayoutProps): JSX.Element => {
    return (
        <>
            <Header />
      
                {children}
  
            <Footer />
        </>
    )
}