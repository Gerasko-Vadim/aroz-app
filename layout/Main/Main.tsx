import React from "react"
import { MainProps } from "./Main.props"
import styles from "./Main.module.css"


export const Main = ({ children }: MainProps): JSX.Element => {
    return (
        <div className={styles.main}>
            {children}
        </div>
    )
}