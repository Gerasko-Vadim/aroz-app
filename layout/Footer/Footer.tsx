import React from "react"
import { Container } from "../../components"
import styles from "./Footer.module.css"
import { FooterProps } from "./Footer.props"
import LogoSvg from "./logo.svg"
import InstagramSvg from "./Instagram.svg"
import YoutubeSvg from "./Youtube.svg"
import MessageSvg from "./Message.svg"
import MobileSvg from "./Mobile.svg"
import LocationSvg from "./Location.svg"

export const Footer = ({ }: FooterProps): JSX.Element => {
    return (
        <footer className={styles.footer}>
            <Container>
                <div className={styles.wrapper}>
                    <div className={styles.aboutBlock}>
                        <LogoSvg className={styles.logo} />
                        <span className={styles.text}>
                            Lorem ipsum dolor amet, consectetur adipiscing elit. Eget nisl nunc quam ac sed turpis volutpat. Cursus sed massa non nisi, placerat.
                        </span>
                        <div className={styles.icons}>
                            <InstagramSvg />
                            <YoutubeSvg />
                        </div>

                    </div>

                    <div className={styles.blockLinks}>
                        <span className={styles.title}>
                            Меню
                        </span>
                        <a>О Нас</a>
                        <a>Новости</a>
                        <a>Исследования</a>
                        <a>Контакты</a>
                        <a>Руководители ОЗ КР</a>
                    </div>
                    <div className={styles.blockLinksCenter}>
                        <a>Галерея</a>
                        <a>НПА</a>
                        <a>Клинические протокола</a>
                    </div>
                    <div className={styles.contactBlock}>
                        <span className={styles.title}>
                            Свяжитесь с нами
                        </span>
                        <span><MessageSvg /> hello@aroz.com</span>
                        <span><MobileSvg /> +91 98765 43210</span>
                        <span><LocationSvg /> Bishkek, 123</span>

                    </div>
                </div>
                <div className={styles.footerBottom}>
                    <div>
                        © 2021 ЦРЗ. All rights reserved
                    </div>
                    <div>
                    </div>
                    <div className={styles.rightBlock}>
                        Terms & Conditions
                        |
                        Privacy Policy
                        |
                        Sitemap
                        |
                        Disclaimer
                    </div>

                </div>
            </Container>
        </footer>
    )
}