import React from "react"
import { Container, Htag } from "../../components"
import styles from "./greetingsBlock.module.css"
import LogoSvg from "./logo.svg"

export const GreetingsBlock = (): JSX.Element => {
    return (
        <div className={styles.greetings}>
            <Container>
                <div className={styles.titleBlock}>
                    <Htag tag="h1">
                    Сайт находится в разработке
                    </Htag>
                </div>
                <div className={styles.content}>
                    <LogoSvg/>
                    <span>
                    Ассоциации РУКОВОДИТЕЛЕЙ Организаций
                     Здравоохранения  Кыргызской Республики 
                    </span>
                </div>
            </Container>
        </div>
    )
}