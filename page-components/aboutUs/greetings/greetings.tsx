import React from "react"
import { Breadcrumb } from 'antd';
import { HomeOutlined } from '@ant-design/icons';
import styles from "./greetings.module.css"
import { Container, Htag } from "../../../components";

const aboutImg = "/about.png"

export const Greetings = (): JSX.Element => {
    return (
        <div className={styles.greetings}>

            <Container >
                <div className={styles.bg}>
                    <>
                        <Breadcrumb separator=">">
                            <Breadcrumb.Item>
                                <HomeOutlined style={{
                                    marginRight: "5px"
                                }} />
                                Главная
                            </Breadcrumb.Item>
                            <Breadcrumb.Item href="">О нас</Breadcrumb.Item>
                        </Breadcrumb>
                    </>
                    <div className={styles.wrapper}>
                        <div className={styles.content}>
                            <Htag tag="h1">
                                О Нас
                            </Htag>
                            <span>
                                Lorem ipsum dolor sit amet, consectetur adipiscing
                                elit. Nunc odio in et, lectus sit lorem id integer.
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                Nunc odio in et, lectus sit lorem id integer. Lorem ipsum
                                dolor sit amet, consectetur adipiscing elit. Nunc odio
                                in et, lectus sit lorem id integer. Lorem ipsum dolor
                                sit amet, consectetur adipiscing elit.
                            </span>
                        </div>
                        <div className={styles.blockImg}>
                            <img alt="img" src={aboutImg} />
                        </div>

                    </div>

                </div>
            </Container>


        </div>
    )
}