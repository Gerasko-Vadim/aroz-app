module.exports = {
	webpack(config, options) {
		config.module.rules.push({
			loader: '@svgr/webpack',
			options: {
				prettier: false,
				svgo: true,
				svgoConfig: {
					plugins: [{ removeViewBox: false }],
				},
				titleProp: true,
			},
			test: /\.svg$/,
		},
    {
      test: /\.(png|jpg|jpeg|gif)$/i,
      type: "asset/resource",
    },);

		return config;
	},
};