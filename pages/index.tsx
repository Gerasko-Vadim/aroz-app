import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import { Layout } from '../layout/layout'

import 'antd/dist/antd.css';
import { Container, Htag, NewCard, SliderHome } from '../components'

const Phone1 = "/iPhone X.png"
const Phone2 = "/iPhone X2.png"
const Phone3 = "/iPhone X3.png"

const doctor = "/doctor.png"
import { LearnCard } from '../components/learnCard/learnCard'
import { DescriptionBlock } from '../layout/descriptionBlock/descriptionBlock';
import { GreetingsBlock } from '../layout/greetingsBlock/greetingsBlock';


const learnCards = [
  <LearnCard key="0" title="Медицинский калкулятор" description="Lorem" img={Phone1} />,
  <LearnCard key="1" title="Медицинский калкулятор" description="Lorem" img={Phone2} />,
  <LearnCard key="2" title="Медицинский калкулятор" description="Lorem" img={Phone3} />,
  <LearnCard key="3" title="Медицинский калкулятор" description="Lorem" img={Phone1} />,

]

const NewCards = [
  <NewCard key="0" date="07 марта 2021 г." title="Встреча министра здравоохранения с актив.." description="Днем  у министра прошла встреча с активистами" img={doctor} />,
  <NewCard key="1" date="07 марта 2021 г." title="Встреча министра здравоохранения с актив.." description="Днем  у министра прошла встреча с активистами" img={doctor} />,
  <NewCard key="2" date="07 марта 2021 г." title="Встреча министра здравоохранения с актив.." description="Днем  у министра прошла встреча с активистами" img={doctor} />,
  <NewCard key="3" date="07 марта 2021 г." title="Встреча министра здравоохранения с актив.." description="Днем  у министра прошла встреча с активистами" img={doctor} />,
  <NewCard key="4" date="07 марта 2021 г." title="Встреча министра здравоохранения с актив.." description="Днем  у министра прошла встреча с активистами" img={doctor} />,

]

const Home: NextPage = () => {
  return (
    <Layout>
      <GreetingsBlock />
      <Container>
        <div>
          <Htag tag="h1">
            Последние новости
          </Htag>
          <SliderHome number={4} cards={NewCards} />
        </div>

        <div>
          <Htag tag="h1">
            Обучение
          </Htag>
          <SliderHome number={3} cards={learnCards} />
        </div>

        <DescriptionBlock />
      </Container>
    </Layout>

  )
}

export default Home
