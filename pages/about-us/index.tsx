
import { NextPage } from "next"
import React from "react"
import { Container } from "../../components"
import { Layout } from "../../layout/layout"
import { Main } from "../../layout/Main/Main"
import { Sidebar } from "../../layout/Sidebar/Sidebar"
import { Greetings } from "../../page-components/aboutUs/greetings/greetings"

const AboutUs: NextPage = () => {
    return (
        <Layout>
            <Greetings />


            <Container>
                <Main>
                    <Sidebar />
                    <div>
                        content
                    </div>
                </Main>
            </Container>

        </Layout>
    )
}

export default AboutUs;