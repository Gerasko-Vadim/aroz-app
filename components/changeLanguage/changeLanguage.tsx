import { Menu, Dropdown } from 'antd'
import React from "react"
import RuSvg from "./ru.svg"
import EnSvg from "./en.svg"
import KgSvg from "./kg.svg"
import ArowSvg from "./rows-down.svg"
import styles from "./changeLanguage.module.css"


const menu = (
    <Menu>
        <Menu.ItemGroup>
            <Menu.Item >
                <div className={styles.item}>
                    <RuSvg />
                    <span>Русский</span>
                </div>
            </Menu.Item>
            <Menu.Item >
                <div className={styles.item}>
                    <KgSvg />
                    <span>
                        Кыргызский
                    </span>
                </div>
            </Menu.Item>
            <Menu.Item>
                <div className={styles.item}>
                    <EnSvg />
                    <span>
                        Английский
                    </span>
                </div>
            </Menu.Item>
        </Menu.ItemGroup>
    </Menu>
)


export const ChangeLanguage = (): JSX.Element => {
    return (
        <Dropdown overlay={menu} placement="bottomLeft" arrow>
            <div className={styles.changelanguage} onClick={e => e.preventDefault()}>
                <RuSvg />  <span>RU</span> <ArowSvg />
            </div>
        </Dropdown>
    )
}