import { DetailedHTMLProps, HTMLAttributes } from "react";

export interface LearnCardProps  {
    img: string;
    title: string;
    description: string
}