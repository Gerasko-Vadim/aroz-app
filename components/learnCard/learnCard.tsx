import React from "react"
import styles from "./learnCard.module.css"
import { LearnCardProps } from "./learnCard.props"

export const LearnCard = ({img, title, description}:LearnCardProps):JSX.Element =>{
    return(
        <div className={styles.card}>
            <div className={styles.blockImg}>
                <img alt="phone" src={img}/>
            </div>
            <span className={styles.title}>
                {title}
            </span>
            <span className={styles.description}>
                {description}
            </span>
        </div>
    )
}