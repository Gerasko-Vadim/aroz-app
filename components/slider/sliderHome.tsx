import React, { useEffect, useRef } from "react"
import Slider from "react-slick";
import styles from "./sliderHome.module.css"

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { SliderHomeProps } from "./sliderHome.props";

import NextArrowSvg from "./nextArrow.svg"
import PrevArrowSvg from "./prevArrow.svg"
import { ButtonForSlider } from '..';




function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
        <NextArrowSvg
            className={styles.nextArrow}
            onClick={onClick}
        />
    );
}

function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
        <PrevArrowSvg
            className={styles.prevArrow}
            onClick={onClick}
        />
    );
}

export const SliderHome = ({ number, cards }: SliderHomeProps): JSX.Element => {


    useEffect(() => {
        const arrowBlock = document.getElementsByClassName('button__bar')
    }, [])
    var settings = {
        dots: true,
        className: "slider variable-width",
        autoplay: true,
        variableWidth: true,
        infinite: true,
        speed: 500,
        slidesToShow: number,
        dotsClass: "button__bar",
        slidesToScroll: 1,
        initialSlide: 0,
        // appendDots: dots => <div className="ft-slick__dots--custom2"><ul>{dots}</ul></div>,

        nextArrow: <SampleNextArrow />,
        prevArrow: <SamplePrevArrow />
        // responsive: [
        //     {
        //         breakpoint: 1024,
        //         settings: {
        //             slidesToShow: 3,
        //             slidesToScroll: 3,
        //             infinite: true,
        //             dots: true
        //         }
        //     },
        //     {
        //         breakpoint: 600,
        //         settings: {
        //             slidesToShow: 2,
        //             slidesToScroll: 2,
        //             initialSlide: 2
        //         }
        //     },
        //     {
        //         breakpoint: 480,
        //         settings: {
        //             slidesToShow: 1,
        //             slidesToScroll: 1
        //         }
        //     }
        // ]
    };
    return (
        <div className={styles.slider}>
            <Slider {...settings}>
                {cards}
            </Slider>
            <div className={styles.bottomBlock}>
                <ButtonForSlider >
                    Весь список
                </ButtonForSlider>
            </div>
        </div>
    )
}