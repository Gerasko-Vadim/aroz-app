import { ReactElement } from "react";

export interface SliderHomeProps {
    cards: ReactElement[];
    number: number
}