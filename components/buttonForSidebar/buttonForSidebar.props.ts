import { DetailedHTMLProps, HTMLAttributes, ReactNode } from "react";

export interface ButtonForSidebarProps extends DetailedHTMLProps<HTMLAttributes<HTMLButtonElement>, HTMLButtonElement>  {
    children: ReactNode;
    active?: boolean;
}