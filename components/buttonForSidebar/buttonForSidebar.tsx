import React from "react"
import cn from "classnames"
import styles from "./buttonForSidebar.module.css"
import { ButtonForSidebarProps } from "./buttonForSidebar.props"

export const ButtonForSidebar = ({ children, active = false, className, ...props }: ButtonForSidebarProps): JSX.Element => {
    return (
        <button
            className={cn(styles.btn, className, {
                [styles.active]: active
            })}
            {...props}
        >
            {children}
        </button>
    )
}