import React from "react"
import cn from "classnames"
import styles from "./container.module.css"
import { ContainerProps } from "./container.props"

export const Container = ({ children, className }: ContainerProps): JSX.Element => {
    return (
        <div
            className={cn(styles.container, className)}
        >
            {children}
        </div>
    )
}