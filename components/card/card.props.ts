import { DetailedHTMLProps } from 'react';

export interface CardProps {
    title: string;
    description: string;
}