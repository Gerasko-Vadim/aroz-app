import React from "react"
import { Htag } from '..'
import styles from "./card.module.css"
import { CardProps } from './card.props'

export const Card = ({ title, description }: CardProps): JSX.Element => {
    return (
        <div className={styles.card}>
            <Htag tag="h1">
                {title}
            </Htag>
            <div className={styles.descriptionBlock}>
                <p>
                    {description}
                </p>
            </div>
        </div>
    )
}