import { DetailedHTMLProps, HTMLAttributes, ReactNode } from "react";

export interface ButtonForSliderProps extends DetailedHTMLProps<HTMLAttributes<HTMLButtonElement>, HTMLButtonElement>  {
    children: ReactNode;
}