import React from "react"
import cn from "classnames"
import styles from "./buttonForSlider.module.css"
import { ButtonForSliderProps } from "./buttonForSlider.props"

export const ButtonForSlider = ({ children, className, ...props }: ButtonForSliderProps): JSX.Element => {
    return (
        <button
            className={cn(styles.btn, className)}
            {...props}
        >
            {children}
        </button>
    )
}