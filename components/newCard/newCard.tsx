import React from "react"
import styles from "./newCard.module.css"
import { NewCardProps } from "./newCard.props"

export const NewCard = ({ img, title, description, date }: NewCardProps): JSX.Element => {
    return (
        <div className={styles.card} style={{width:"248px"}}>
            <div className={styles.blockImg}
                style={{
                    backgroundImage: `url(${img})`
                }}
            >
            </div>
            <div className={styles.content}>
                <span className={styles.date}>
                    {date}
                </span>
                <span className={styles.title}>
                    {title}
                </span>
                <span className={styles.description}>
                    {description}
                </span>

                <a>Читать полностью</a>
            </div>

        </div>
    )
}