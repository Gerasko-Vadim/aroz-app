import { DetailedHTMLProps, HTMLAttributes } from "react";

export interface NewCardProps  {
    img: string;
    title: string;
    date: string;
    description: string
}